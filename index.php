<?php

session_start();

require_once 'system/configuration.php';
require_once 'system/classes/App.php';
require_once 'system/classes/User.php';
require_once 'blocks/header.php';
if(isset($_GET['page'])) {
    switch ($_GET['page']) {
        case "main":
            require_once "pages/main.php";
            break;
        case "users":
            require_once "pages/users_list.php";
            break;
        case "user_view":
            require_once "pages/user_view.php";
            break;
        case "user_edit":
            require_once "pages/user_edit.php";
            break;
        default:
            require_once "pages/404.php";
    }
}else{
    echo "<script>location.replace('/?page=main');</script>";
}
require_once 'blocks/footer.php';
?>