</div>
</main>
<footer class="footer mt-auto py-3">
    <div class="container">
        <p class="text-muted"><?php echo App::getName(). ' '. date('Y'). ' ' . '&copy;'; ?></p>
        <p class="text-muted"><?php echo App::getAddress(); ?></p>
        <p class="text-muted"><?php echo App::getEmail(); ?></p>
        <p class="text-muted"><?php echo App::getPhone(); ?></p>
    </div>
</footer>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="/assets/js/jquery-3.6.0.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>


</body>
</html>