<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
<!--    <link rel="stylesheet" href="/assets/css/bootstrap-grid.min.css">-->
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <link href="/assets/css/sticky-footer-navbar.css" rel="stylesheet">
    <title><?php echo App::getName(). '-'. App::getSlogan();  ?></title>
</head>
<body class="d-flex flex-column h-100">
<header>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><?php echo App::getName(); ?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/?page=users">Users List</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-expanded="false">
                    Dropdown
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled">Disabled</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <?php
            if (isset($_SESSION['username']) && $_SESSION['uid']) {
                $user_obj = new User();
                $user = $user_obj->getUserById($_SESSION['uid']);
                $user_first_name = $user['first_name'];
                $user_last_name = $user['last_name'];

                ?>
                <span class="text-white"><?php echo $user_first_name . '&nbsp;' . $user_last_name; ?></span>&nbsp;&nbsp;&nbsp;

                <?php
                    if($user['status'] == "admin"){
                        echo '<a class="btn btn-outline-info my-2 my-sm-0" href="/admin_panel/">Admin panel</a>';
                    }
                ?>

                <a class="btn btn-outline-danger my-2 my-sm-0" href="/logout.php">Log out -></a>
            <?php } else { ?>
                <a class="btn btn-outline-success my-2 my-sm-0" href="/login.php">Login</a> &nbsp;&nbsp;&nbsp;
                <a class="btn btn-outline-primary my-2 my-sm-0" href="/register.php">Register</a>
            <?php } ?>
        </form>
    </div>
</nav>
</header>
<main class="flex-shrink-0 flex-fill">
    <div class="container">